import requests
import xml.etree.ElementTree as ET
import time
import re
from datetime import date, timedelta
import json

def lambda_handler(event,contex):
    #Get ALL devices
    url = 'https://cn556.awmdm.com/API/v1/mdm/devices/search?user=%s' %('')
    headers= {'Authorization':'Basic cGEua2VtcGY6RDNtb2NyYXRpZUNoYXQ=','aw-tenant-code': 'Z6BCk2GZCaHX5AuvTssD0GH7d3WQkMgdM+eQbuCm17Y='}
    response = requests.get(url, headers=headers)
    data = re.sub(r'xmlns="http://www.air-watch.com/servicemodel/resources"',r'',response.text)

    #Parse data and send it to splunk
    root = ET.fromstring(data.encode('utf-8'))
    for child in root.iter('Devices'):
        #Splunk UI is accessed on http://host:8000
        #We build the json data with the informations then send it to splunk Http Listener
        splunkhost = '52.51.232.37'
        splunkUrl = 'https://%s:8090/services/collector/event' % splunkhost
        splunkHeaders = {'Authorization': 'Splunk 79D5B1D6-F300-424F-9479-2BEEE1FD83FE'}
        splunkData = {}
        splunkData['index'] = 'client-cloudreach'
        splunkData['source'] = 'Airwatch'

        #We build the event with the list of data we want to send
        event = {}
        event['Id'] = child[0].text
        event['SerialNumber'] = child[2].text
        event['DisplayName'] = child[10].get('title')
        event['Name'] = child[11].text
        event['Email'] = child[12].text
        event ['LastSeen'] = child[20].text
        splunkData['event'] = event
        json_splunkData = json.dumps(splunkData)
        #Finally we send the event
        #We send an event per Device found as to facilitate searches on Splunk
        splunkResponse = requests.post(splunkUrl, headers=splunkHeaders, data=json_splunkData, verify=False)
        #Make sure port used is free (telnet -ano|grep PORT|grep LISTEN)
        #If not, change port, or free port
